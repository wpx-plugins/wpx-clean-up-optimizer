<?php

namespace WpExperts\WpxCleanUpOptimizer;

/**
 * Cleans pp and optimizes WordPress
 * @package WPExperts\WPXCleanUpOptimizer
 */
class WpCleanUpOptimizer
{
    private static $instance;

    public static function getInstance()
    {
        if (null === self::$instance) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public function __construct()
    {
        $this->setupHooks();

        (new WpRestApi());
    }

    public function setupHooks()
    {
        add_action('after_theme_support', [$this, 'removeThemeSupport']);
        add_action('init', [$this, 'wpHeadCleanUp']);
        add_filter('xmlrpc_enabled', '__return_false');
        add_action('wp_default_scripts', [$this, 'removeJqueryMigrate']);
    }

    public function removeEmojiStyles()
    {
        remove_action('wp_print_styles', 'print_emoji_styles');
    }

    public function removeThemeSupport()
    {
        remove_theme_support('automatic-feed-links');
    }

    public function wpHeadCleanUp()
    {
        remove_action('wp_head', 'print_emoji_detection_script', 7);
        remove_action('wp_head', 'feed_links_extra', 3);
        remove_action('wp_head', 'feed_links', 2);
        remove_action('wp_head', 'rsd_link');
        remove_action('wp_head', 'wlwmanifest_link');
        remove_action('wp_head', 'index_rel_link');
        remove_action('wp_head', 'parent_post_rel_link', 10, 0);
        remove_action('wp_head', 'start_post_rel_link', 10, 0);
        remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
        remove_action('wp_head', 'wp_generator');
    }

    public function removeJqueryMigrate($scripts)
    {
        if (!is_admin() && isset($scripts->registered['jquery'])) {
            $script = $scripts->registered['jquery'];

            if ($script->deps) {
                $script->deps = array_diff($script->deps, array(
                    'jquery-migrate'
                ));
            }
        }
    }
}
