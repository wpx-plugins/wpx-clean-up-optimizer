<?php

namespace WpExperts\WpxCleanUpOptimizer;

/**
 * Loads WordPress REST API settings
 * @package WPExperts\WPXCleanUpOptimizer
 */
final class WpRestApi
{
    public function __construct()
    {
        $this->setupHooks();
    }
    
    /**
     * Setups hooks
     * @return void
     */
    public function setupHooks()
    {
        $this->cleanUp();
        add_filter('rest_endpoints', [$this, 'disableUserEndpoints']);
    }
    
    /**
     * Cleans up some REST API settings
     * @return void
     */
    public function cleanUp()
    {
        remove_action('template_redirect', 'rest_output_link_header', 11, 0);
        remove_action('wp_head', 'rest_output_link_wp_head', 10);
        remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
        
        add_filter('embed_oembed_discover', '__return_false');
    }
    
    /**
     * Disables user REST API endpoints
     * @param mixed $endpoints
     * @return mixed
     */
    public function disableUserEndpoints($endpoints)
    {
        if (isset($endpoints['/wp/v2/users'])) {
            unset($endpoints['/wp/v2/users']);
        }
        if (isset($endpoints['/wp/v2/users/(?P<id>[\d]+)'])) {
            unset($endpoints['/wp/v2/users/(?P<id>[\d]+)']);
        }
        
        return $endpoints;
    }
}
