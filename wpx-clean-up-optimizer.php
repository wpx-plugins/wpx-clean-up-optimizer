<?php

/**
 * Plugin Name:  WordPress Clean Up And Optimizer
 * Description:  WordPress Clean Up And Optimizer.
 * Version:      1.0.0
 * Author:       WP Experts
 * Author URI:   https://wpexperts.be
 * Text Domain:  wpx-clean-up-optimizer
 * Domain Path:  /languages/
 * Update URI: false
 * Requires PHP: 7.4
 */

// Load bootstrap instance
\WpExperts\WpxCleanUpOptimizer\WpCleanUpOptimizer::getInstance();
